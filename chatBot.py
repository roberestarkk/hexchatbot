import hexchat
import time
import threading
import urllib.request
import os
import sys
duckFile = os.getenv('APPDATA') + "\\Hexchat\\addons\\hexchatbot"
sys.path.append(duckFile)
import lib_duckduckgo

__module_name__ = "ChatBot"
__module_version__ = "2.92"
__module_description__ = "Has functionality triggered by banged keywords (!word)"

hexchat.prnt('ChatBot script loading')

''' The prefix people use when interacting with the bot '''
botPrefix = '.'

''' Global non-static variable to allow for delaying the functionality of the script becoming active '''
isActive = False

''' Constants representing indexes for the features '''
START_DELAY = 20.0
SPAM_DELAY = 10.0
FEAT_CAT = 0
FEAT_MERICA = 1
FEAT_FRY = 2
FEAT_SHALLOT = 3
FEAT_NEKKID = 4
FEAT_GRENADIERS = 5
FEAT_SILENCE = 6
FEAT_BUTT = 7
FEAT_WHAT = 8
FEAT_RHAPSODY = 9
FEAT_NYCKEL = 10
FEAT_PORN = 11
FEAT_BOOP = 12
FEAT_HIFIVE = 13
FEAT_DOG = 14
FEAT_SMASHING = 15
FEAT_DILLIGAF = 16
FEAT_AUSSIE = 17
FEAT_RELIGION = 18
FEAT_WOLVES = 19
FEAT_FLUFFY = 20
FEAT_HERETIC = 21
FEAT_DUCK = 22
NUM_FEATURES = 23

''' Set up an array containing a value indexed at 'FEAT_...'*2 for the last time since command called and 'FEAT_...'*2+1 for last time apologised for denying command
	to prevent spam of commands '''
timenow = time.time()-SPAM_DELAY
timeoutList = [timenow]*NUM_FEATURES*2

''' An array of keywords to look for and assign the feat ID's from '''
''' The Duck Duck Go keywords have a space on the end on purpose, this is to prevent partial word matches (like go in good) '''
keywordList = [
['cat','kitt','nyan', 'sad','meow'],
['merica'],
['fry','treasure'],
['shallot'],
['nekkid','naked'],
['brit', 'grenadier', 'tea'],
['silence','shush','quiet'],
['butt','pron'],
['what'],
['bohemian','rhapsody'],
['nyckelharpa'],
['porn'],
['boop'],
['five'],
['dog'],
['smash', 'nigel', 'thornberry'],
['idc','dilligaf'],
['aussie','oi'],
['religion'],
['wolf','wolv','zevon','london','ahoo'],
['fluff'],
['heretic','heresy','blasphem'],
['duck ','ddg ','go '],
]

commandList = [
"http://thecatapi.com/api/images/get?format=src", 
"timer 0.5 say [Bot] https://www.youtube.com/watch?v=bBHNsc5EL1Q", 
"timer 0.5 say [Bot] https://www.youtube.com/watch?v=RHxXcPC-PcM",
"timer 0.5 say [Bot] https://www.youtube.com/watch?v=2i13rHJyCOY",
"timer 0.5 say [Bot] http://media-cache-ec0.pinimg.com/736x/9a/c5/fd/9ac5fd1d11e22420a7b029ba324a256c.jpg",
"timer 0.5 say [Bot] https://www.youtube.com/watch?v=7wiENQMc9g8",
"timer 0.5 say [Bot] https://www.youtube.com/watch?v=m66Zn4OXaGM",
"timer 0.5 say [Bot] http://goo.gl/91XjuP",
"timer 0.5 say [Bot] https://www.youtube.com/watch?v=W-jOzUjd5i4",
"timer 0.5 say [Bot] https://www.youtube.com/watch?v=XWE7boPU6kI",
"timer 0.5 say [Bot] https://www.youtube.com/watch?v=ERaiL_2PURs",
"timer 0.5 say [Bot] https://www.youtube.com/watch?v=eWEjvCRPrCo",
"timer 0.5 say [Bot] http://4.bp.blogspot.com/-sQWml-5kgfs/T0h_CTDTvEI/AAAAAAAAqDY/V2br-k3WRWo/s1600/Funny+Dog+Pictures+With+Captions+%28130%29.jpg",
"timer 0.5 say [Bot] http://blog.chron.com/tubular/files/2015/01/High-five-colbert.gif",
"timer 0.5 say [Bot] https://vine.co/v/OUwnaOzv7UD",
"timer 0.5 say [Bot] http://static.damnlol.com/pics/343/b43b2789ee6d62244c10fd27862acefa.gif",
"timer 0.5 say [Bot] [NSFW] https://www.youtube.com/watch?v=7a6EOyaMdqY",
"timer 0.5 say [Bot] [NSFW] https://www.youtube.com/watch?v=7Ijahng-WCQ",
"timer 0.5 say [Bot] [NSFW] https://www.youtube.com/watch?v=qcKye-5iMCk",
"timer 0.5 say [Bot] https://www.youtube.com/watch?v=iDpYBT0XyvA",
"timer 0.5 say [Bot] https://www.youtube.com/watch?v=1SI6rhotAkE",
"timer 0.5 say [Bot] http://i0.kym-cdn.com/photos/images/original/000/691/973/e75.jpg",
"USE THE DUCKDUCKGO QUERY LIBRARY INSTEAD OF THE API URL YOU DUMMY",
]

featureList = [
{'name': "Cat", 'next': "Adorable Cat Picture"}, 
{'name': "Disparaging Eyeroll at Americans", 'next': "Eyeroll"}, 
{'name': "National Fucking Treasure", 'next': "Stephen"}, 
{'name': "Shallot", 'next': "Shallot"},
{'name': "Super Hot Gay Sex Scene", 'next': "Nekkid Dude"},
{'name': "Kawaii British Grenadier", 'next': "Grenadier"},
{'name': "Threatening Terrorist", 'next': "Achmed"},
{'name': "Random Butt Pic", 'next': "Butt"},
{'name': "What the Damn Hell", 'next': "What"},
{'name': "The Real Life", 'next': "Just Fantasy"},
{'name': "Nyckelharpa Rock", 'next': "Skimrande"},
{'name': "Random Porn Post", 'next': "Porn Post"},
{'name': "NOSE BOOP ERMAHGERD", 'next': "Boop"},
{'name': "Monsieur Colbert!", 'next': "Hi Five"},
{'name': "Dog Swarm", 'next': "Doggies"},
{'name': "Jolly Smashing", 'next': "Thornberry"},
{'name': "Fuck to Not Give", 'next': "DILLIGAF"},
{'name': "Aussie Sporting Warcry", 'next': "Medley"},
{'name': "Religious Detriment", 'next': "Festival"},
{'name': "Werewolf of London", 'next': "Ahooooooooo!"},
{'name': "Fluffy Thing", 'next': "FLUFFAAAY"},
{'name': "Heretical Panic", 'next': "Blasphemy"},
{'name': "Query", 'next': "Search"},
]

''' Handles all the various complexities of the feature response system '''
def doCommand(commID, wordTest):
	command = commandList[commID]

	''' Checks the timeout value of the feature param ID, and if less than the current time + the delay value, executes the associated command '''
	if (timeoutList[commID*2] + SPAM_DELAY < time.time()):
		if commID == FEAT_CAT:
			command = doCat(wordTest)
		hexchat.command(command)
		timeoutList[commID*2] = time.time()
		''' Checks the timeout value of the featureSorry param ID, and if less than the current time + the delay value, executes the associated commands '''
	elif (timeoutList[commID*2+1] + SPAM_DELAY < time.time()):
		hexchat.command("timer 0.5 say Sorry, only one " + str(featureList[commID]['name']) + " allowed per " + str(int(SPAM_DELAY)) + " seconds!")
		hexchat.command("timer 0.5 say " + str(int((timeoutList[commID*2] + SPAM_DELAY) - time.time())) + " seconds till next " + str(featureList[commID]['next']))
		timeoutList[commID*2+1] = time.time()

def sendHelp(nickname):
	''' Strip the first 3 characters of the nickname because it is IRC colour codes '''
	nickname = nickname[3:]
	hexchat.command("timer 0.5 notice " + nickname + " Welcome to Robere_Starkk's ChatBot!")
	hexchat.command("timer 0.5 notice " + nickname + " To use this chatbot, start your message using the '"+ botPrefix+ "' character.")
	hexchat.command("timer 0.5 notice " + nickname + " This will allow the chatbot to see that you're talking to it.")
	hexchat.command("timer 0.5 notice " + nickname + " After it sees that '" + botPrefix + "' character, anything you type in after it in that same message (spaces or no spaces)")
	hexchat.command("timer 0.5 notice " + nickname + " will be searched by the chatbot for a few key-words that it uses to determine how it will respond.")
	hexchat.command("timer 0.5 notice " + nickname + " ----------")
	hexchat.command("timer 0.5 notice " + nickname + " Those key-words and what they do, are as follows:")
	hexchat.command("timer 0.5 notice " + nickname + " - 'help': This is only shown if you specifically type '!help' as the very beginning of your message.")
	hexchat.command("timer 0.5 notice " + nickname + " 		This is a fairly standard naming convention, and so is implemented this narrowly to allow people the")
	hexchat.command("timer 0.5 notice " + nickname + " 		ability to populate their other chatbot commands with pseudo-meaningful filler")
	hexchat.command("timer 0.5 notice " + nickname + " - 'cat' or 'kitt' or 'nyan' or 'sad' or 'meow': Specifies that you want the link to a random image of a cat.")
	hexchat.command("timer 0.5 notice " + nickname + " 		This can be further refined by 'gif' or 'pic' to limit the image to animated or not.")
	hexchat.command("timer 0.5 notice " + nickname + " 		It will freely pick either if you don't specify.")
	hexchat.command("timer 0.5 notice " + nickname + " - 'merica': Specifies that you want the link to that one video Rob is always using from The Mummy (1999)")
	hexchat.command("timer 0.5 notice " + nickname + " 		that contains a stereotypical British dude disparagingly rolling his eyes and sighing 'Americans'.")
	hexchat.command("timer 0.5 notice " + nickname + " - 'shallot': SHALLOTS!")
	hexchat.command("timer 0.5 notice " + nickname + " - 'treasure' or 'fry': Because Stephen Fry is a National Fucking Treasure!")
	hexchat.command("timer 0.5 notice " + nickname + " - 'naked' or 'nekkid': For all your super sexy gay man sex needs!")
	hexchat.command("timer 0.5 notice " + nickname + " - 'grenadier' or 'brit' or 'tea': Anime Girl sipping tea and listening to British Grenadiers for 10 hours")
	hexchat.command("timer 0.5 notice " + nickname + " - 'silence' or 'shush' or 'quiet': Achmed threatening you to be quiet or else")
	hexchat.command("timer 0.5 notice " + nickname + " - 'butt': Pic of a butt")
	hexchat.command("timer 0.5 notice " + nickname + " - 'what': The damn hell!?")
	hexchat.command("timer 0.5 notice " + nickname + " - 'bohemian' or 'rhapsody': Beelzebub has a devil put aside for me!")
	hexchat.command("timer 0.5 notice " + nickname + " - 'nyckelharpa': Badass Nyckelharpa Rock music")
	hexchat.command("timer 0.5 notice " + nickname + " - 'porn': Used to be a Random link to pornographic blog post")
	hexchat.command("timer 0.5 notice " + nickname + " - 'boop': ADORABLE BOOP PIC")
	hexchat.command("timer 0.5 notice " + nickname + " - 'five': Get a High Five")
	hexchat.command("timer 0.5 notice " + nickname + " - 'dog': Summons dogs")
	hexchat.command("timer 0.5 notice " + nickname + " - 'smash' or 'nigel' or 'thornberry': Smashing Brie old Chap!")
	hexchat.command("timer 0.5 notice " + nickname + " - 'idc' or 'dilligaf': [NSFW] Well Do I?")
	hexchat.command("timer 0.5 notice " + nickname + " - 'aussie' or 'oi': [NSFW] BOOMERANGA-CHUCKA-CHUCKA")
	hexchat.command("timer 0.5 notice " + nickname + " - 'religion': [NSFW] Bible Bashing Festival!")
	hexchat.command("timer 0.5 notice " + nickname + " - 'wolf' or 'zevon' or 'london' or 'ahoo': His hair was perfect!")
	hexchat.command("timer 0.5 notice " + nickname + " - 'fluff': It's so fluffay!")
	hexchat.command("timer 0.5 notice " + nickname + " - 'heresy' or 'heretic' or 'blasphem': Provides adorable denouncement of Heresy")
	hexchat.command("timer 0.5 notice " + nickname + " - 'duck' or 'ddg' or 'go': Takes everything after the keyword and uses it as a search query in Duck Duck Go")

def duckQuery(wordTest):
	theComm = "timer 0.5 say [DuckDuckGo] " + lib_duckduckgo.get_zci(wordTest)
	hexchat.command(theComm)

def doCat(wordTest):
	fileType = ""

	if wordTest.find('gif') > 0:
		fileType = "&type=gif"
	elif wordTest.find('pic') > 0:
		fileType = "&type=jpg,png"
	else:
		fileType = ""
	try:
		url = urllib.request.urlopen(commandList[FEAT_CAT] + fileType)
	except:
		return "timer 0.5 say [Bot] Connection Failed, please try again"
	else:
		return "timer 0.5 say [Bot] " + url.geturl()

def process_command(word, word_eol, userdata, attributes):
	if isActive:
		''' Help command special case '''
		if word[1].startswith(botPrefix + 'help'):
			sendHelp(word[0])
		for test in keywordList[FEAT_DUCK]:
			if word[1].startswith(botPrefix+test):
				wordTest = word[1]
				newWord = wordTest.replace(botPrefix+test,"")
				duckQuery(newWord)
				break

		wordTest = word_eol[1].lower()
		''' Checking for ! command delineator '''
		if wordTest.startswith(botPrefix):

			ID = -1

			for featID in range(0,NUM_FEATURES):
				for featTrigger in keywordList[featID]:
					if (wordTest.find(featTrigger) > 0):
						ID = featID
						break

			''' Explicitly exclude the duck duck go keywords from being considered as a regular match '''
			if ID != -1 and ID != FEAT_DUCK:
				doCommand(ID, wordTest)

			return hexchat.EAT_PLUGIN

		return hexchat.EAT_NONE

def setActive():
	global isActive
	isActive = True
	hexchat.prnt("Chatbot Active")
	return hexchat.EAT_NONE

def startTimeout(word, word_eol, userdata):
	delayActivation()
	
def delayActivation():
	isActive = False
	hexchat.prnt("Triggered Delay Timer")
	t = threading.Timer(START_DELAY, setActive)
	t.start()
	return hexchat.EAT_NONE

delayActivation()

hexchat.hook_print("Connected", startTimeout, priority=hexchat.PRI_HIGHEST)

hexchat.hook_print_attrs("Channel Message", process_command, priority=hexchat.PRI_HIGHEST)
hexchat.hook_print_attrs("Your Message", process_command, priority=hexchat.PRI_HIGHEST)

hexchat.prnt("ChatBot Loaded!")